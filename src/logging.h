#ifndef LOGGING_H
#define LOGGING_H

#include <iostream>

template<typename... T>
void println(T&&... args)
{
    (std::cout << ... << args) << '\n';
}

enum class LogType : int {
    info,
    warn,
    error,
};

template<typename... T>
void logln(LogType logType, T&&... args)
{
    switch (logType) {
    case LogType::info:
        std::cout << "[INFO] ";
        break;

    case LogType::warn:
        std::cout << "[WARN] ";
        break;

    case LogType::error:
        std::cout << "[ERROR] ";
        break;
    }

    println(std::forward<T>(args)...);
}

class LogItem
{
private:
    LogType m_logType;
    std::string m_text;

public:
    LogItem(LogType logType, std::string text)
        : m_logType(logType),
          m_text(std::move(text))
    {}

public:
    void what() const
    {
        logln(m_logType, m_text);
    }
};

#endif // LOGGING_H
