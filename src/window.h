#ifndef WINDOW_H
#define WINDOW_H

#include <string>

struct GLFWwindow;

class Window
{
private:
    int m_windowWidth;
    int m_windowHeight;
    std::string m_windowTitle;
    GLFWwindow* m_window = nullptr;

public:
    explicit Window(int width = 800, int height = 600, std::string title = "");
    ~Window();

public:
    static void frameBufferResizeCallback(GLFWwindow*, int newWidth, int newHeight);
    void getInput();
    void preRender();
    void render();
    void show();
};

#endif // WINDOW_H
