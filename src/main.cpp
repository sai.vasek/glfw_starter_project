#include "logging.h"
#include "window.h"

int main()
{
    try {
        auto window = Window();
        window.show();
    }

    catch (const LogItem& error) {
        error.what();
    }

    return 0;
}
