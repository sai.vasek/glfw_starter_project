// clang-format off
#include "window.h"
#include "logging.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
// clang-format on

Window::Window(int width, int height, std::string title)
    : m_windowWidth(width),
      m_windowHeight(height),
      m_windowTitle(std::move(title))
{
    if (!glfwInit())
        throw LogItem(LogType::error, "Could not initialize GLFW!");

    m_window = glfwCreateWindow(m_windowWidth, m_windowHeight, m_windowTitle.c_str(), nullptr, nullptr);
    glfwSwapInterval(1);

    if (!m_window)
    {
        glfwTerminate();
        throw LogItem(LogType::error, "Could not create GLFW window!");
    }

    glfwMakeContextCurrent(m_window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        glfwTerminate();
        throw LogItem(LogType::error, "Could not initialize GLAD!");
    }

    glViewport(0, 0, m_windowWidth, m_windowHeight);
    glfwSetFramebufferSizeCallback(m_window, frameBufferResizeCallback);
}

Window::~Window()
{
    glfwTerminate();
}

void Window::frameBufferResizeCallback(GLFWwindow*, int newWidth, int newHeight)
{
    glViewport(0, 0, newWidth, newHeight);
    logln(LogType::info, "Frame buffer resized. New dimensions: ", newWidth, 'x', newHeight);
}

void Window::getInput()
{
    if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(m_window, true);
}

void Window::preRender()
{

}

void Window::render()
{

}

void Window::show()
{
    preRender();

    while (!glfwWindowShouldClose(m_window))
    {
        getInput();
        glfwPollEvents();
        render();
        glfwSwapBuffers(m_window);
    }
}
