This is a starter project for quickly initializing platform-independent OpenGL projects through GLFW and GLAD. You need to supply your own GLAD implementation. 
- Get it from: https://glad.dav1d.de/
- Place include and src directories inside ./vendor/glad
